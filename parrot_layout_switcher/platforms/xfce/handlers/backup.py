"""
Handle backup configurations of current layout for XFCE. The backup will be triggered when user select different layout
And current layout is not listed in layout list
"""
import os
import json
import importlib
from parrot_layout_switcher.platforms.xfce import layouts
from parrot_layout_switcher.platforms.xfce import xfconf

XFCONF = xfconf.XfconfSetting()


def need_backup():
    def check_single_layout(module_object):
        if hasattr(module_object, "XFCELayout"):
            for setting in getattr(module_object, "XFCELayout")().settings:
                # for setting in layout_settings:
                # Syntax:
                #   setting[0]: channel
                #   setting[1]: property
                #   setting[2]: value
                if XFCONF.xfconf.PropertyExists(setting[0], setting[1]):
                    if XFCONF.xfconf.GetProperty(setting[0], setting[1]) != setting[2]:
                        # return to do backup
                        return True
                else:
                    # return to do backup
                    return True
            # No need backup
            return False

    for layout_name in layouts.get_layouts(layouts.__path__[0] + "/"):
        module = importlib.import_module(f"xfce4_layout_changer.layouts.{layout_name}")
        if not check_single_layout(module):
            return False
    return True


def save_backup(data):
    backup_directory = os.path.expanduser("~") + "/.xfce_layouts/"
    if not os.path.exists(backup_directory):
        os.makedirs(backup_directory)

    from datetime import datetime
    file_name = datetime.now().strftime("%Y.%m.%d_%H_%M_%S_%s")
    with open(backup_directory + file_name, "w") as backup_settings:
        json.dump(data, backup_settings)
