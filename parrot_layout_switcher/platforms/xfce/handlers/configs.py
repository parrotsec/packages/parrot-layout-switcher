"""
Change configs of whisker menu plugin and system load plugin by overwrite configuration at
$HOME/.config/xfce4/panel
"""
import os
import configparser
from parrot_layout_switcher.platforms.xfce import resources

XFCE_CONFIG_DIR = os.path.expanduser("~/.config/xfce4/panel")
RESOURCES = resources.__path__[0]


def whisker_menu_config(config_id, show_title=True, windows_theme=False):
    """
    :param config_id: int -> The id number of whisker menu was generated at config folder
    :param show_title: Change setting of whisker menu to show title at the right of icon or not
    :param windows_theme: bool -> Change icon to windows
    :return:
    """
    # whisker menu configurations:
    # button-title=<title_name>
    # button-icon=<icon_name>
    # show-button-title=<true|false>
    # show-button-icon=<true|false>
    # load-hierarchy=true Show tree
    # whisker menu file name configurations:
    # whiskermenu-1.rc: top_only, bottom_only, dock, top_left, top_right, windows_10, kde
    # whiskermenu-2.rc: windows_11
    # whiskermenu-6.rc: traditional2
    # whiskermenu-8.rc: dock and top
    # whiskermenu-9.rc: traditional
    # whiskermenu-11.rc: gnome

    if windows_theme:
        button_icon_name = "xfce4-panel-menu"
    else:
        button_icon_name = "view-list"
    config = configparser.RawConfigParser()

    gen_config = ""
    full_config_path = XFCE_CONFIG_DIR + "/" + f"whiskermenu-{config_id}.rc"
    if os.path.exists(full_config_path):
        config_data = open(full_config_path).read()
    else:
        config_data = open(RESOURCES + "/whiskermenu.rc").read()

    config.read_string(f"[dummysection]\n{config_data}")
    config["dummysection"]["show-button-title"] = "true" if show_title else "false"
    config["dummysection"]["button-icon"] = button_icon_name
    config["dummysection"]["button-title"] = "Menu"
    config["dummysection"]["load-hierarchy"] = "true"
    # Overwrite favorite menu if whiskermenu is using default
    if config["dummysection"]["favorites"] == "xfce4-web-browser.desktop,xfce4-mail-reader.desktop," \
                                              "xfce4-file-manager.desktop,xfce4-terminal-emulator.desktop":
        config["dummysection"]["favorites"] = "update-reminder.desktop,mousepad.desktop,encryptpad.desktop," \
                                              "anon-gui.desktop,codium.desktop,org.remmina.Remmina.desktop,htop.desktop"

    for section in config.sections():
        if section != "dummysection":
            gen_config += f"\n[{section}]\n"
        for items in config.items(section):
            gen_config += f"{items[0]}={items[1]}\n"

    with open(full_config_path, "w") as write_config:
        write_config.write(gen_config)


def sysload_config():
    config_data = open(RESOURCES + "/systemload.rc").read()

    for config_file in os.listdir(XFCE_CONFIG_DIR):
        if config_file.startswith("systemload") and config_file.endswith(".rc"):
            full_config_path = XFCE_CONFIG_DIR + "/" + config_file
            # Only overwrite if uptime is enabled
            if "[SL_Uptime]\nEnabled=true" in open(full_config_path).read():
                with open(full_config_path, "w") as write_config:
                    write_config.write(config_data)
