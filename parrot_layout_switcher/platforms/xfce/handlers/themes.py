# This file is part of the Zorin Appearance program.
#
# Copyright 2016-2019 Zorin OS Technologies Ltd.
# Based on code from gnome-tweak-tool by John Stowers, Copyright 2011.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.

import os
from parrot_layout_switcher.platforms.xfce import xfconf
XFCONF = xfconf.XfconfSetting()


def get_current_theme():
    return XFCONF.xfconf.GetProperty("xsettings", "/Net/ThemeName")


def get_current_icon_theme():
    return XFCONF.xfconf.GetProperty("xsettings", "/Net/IconThemeName")


def get_wm_theme():
    return XFCONF.xfconf.GetProperty("xfwm4", "/general/theme")


def set_theme(theme):
    XFCONF.xfconf.SetProperty("xsettings", "/Net/ThemeName", theme)


def set_icon_theme(theme):
    XFCONF.xfconf.SetProperty("xsettings", "/Net/IconThemeName", theme)


def set_wm_theme(theme):
    XFCONF.xfconf.SetProperty("xfwm4", "/general/theme", theme)


def set_windows_theme():
    if os.path.exists("/usr/share/themes/Windows 10 Dark"):
        theme_name = "Windows 10 Dark"
    else:
        theme_name = "Windows 10 Light"
    set_theme(theme_name)
    set_wm_theme(theme_name)
    set_icon_theme("Windows-10-Icons")


def set_linux_theme():
    if get_current_theme().startswith("Windows "):
        set_theme("Icy-Dark")
        set_wm_theme("Icy-Dark")
        set_icon_theme("ara")
