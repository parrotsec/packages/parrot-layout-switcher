import gi
from parrot_layout_switcher.platforms.xfce.layouts import *

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class XFCELayout(Layout, Gtk.Box):
    __metaclass__ = LayoutBox

    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        super().__init__()
        self.show_title = False
        self.layout_settings = [
            {
                "settings": {"mode": 2, "size": 48, "pos": "bottom_right", "autohide": 0},
                "plugins": (
                    {"whisker_menu": {}},
                    {"task_list": {"show_labels": False, "grouping": True}},
                    {"separator": {"expand": True}},
                    {"pulseaudio": {}},
                    {"systray": {}},
                    {"clock": {"style": 2}},
                )
            }
        ]
        self.settings = self.settings_to_xfconf()

        label = Gtk.Label()
        label.set_text("Top Right")

        preview = Gtk.DrawingArea()
        preview.connect('draw', self.draw_layout_preview)
        preview.set_size_request(LAYOUT_PREVIEW_WIDTH, LAYOUT_PREVIEW_HEIGHT)

        self.pack_start(preview, True, False, 0)
        self.pack_start(label, True, False, 0)

    def draw_layout_preview(self, widget, cr):
        style_context = widget.get_style_context()
        color = style_context.get_color(Gtk.StateFlags.NORMAL)
        cr.set_source_rgba(*color)

        # Outline
        cr.set_line_width(2)
        cr.rectangle(1, 1, LAYOUT_PREVIEW_WIDTH - 2, LAYOUT_PREVIEW_HEIGHT - 2)
        cr.stroke()

        # Panel
        cr.set_line_width(2)
        cr.move_to(LAYOUT_PREVIEW_WIDTH - 13, 2)
        cr.line_to(LAYOUT_PREVIEW_WIDTH - 13, LAYOUT_PREVIEW_WIDTH - 2)
        cr.stroke()
        cr.move_to(0, 0)

        # Menu
        cr.set_line_width(2)
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 76, 4, 60, 70)
        cr.stroke()

        # Menu icon 1
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 45, 7, 4, 4)
        cr.fill()

        # Menu text 1
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 38, 8, 15, 2)
        cr.fill()

        # Menu icon 2
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 45, 15, 4, 4)
        cr.fill()

        # Menu text 2
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 38, 16, 10, 2)
        cr.fill()

        # Menu icon 3
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 45, 23, 4, 4)
        cr.fill()

        # Menu text 3
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 38, 24, 12, 2)
        cr.fill()

        # Menu icon 4
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 45, 31, 4, 4)
        cr.fill()

        # Menu text 4
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 38, 32, 11, 2)
        cr.fill()

        # Menu icon 5
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 45, 39, 4, 4)
        cr.fill()

        # Menu text 5
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 38, 40, 9, 2)
        cr.fill()

        # Menu icon 6
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 45, 47, 4, 4)
        cr.fill()

        # Menu text 6
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 38, 48, 14, 2)
        cr.fill()

        # Menu icon 7
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 45, 55, 4, 4)
        cr.fill()

        # Menu text 7
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 38, 56, 8, 2)
        cr.fill()

        # Menu icon 8
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 45, 63, 4, 4)
        cr.fill()

        # Menu text 8
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 38, 64, 10, 2)
        cr.fill()

        # Menu right text 1
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 70, 8, 10, 2)
        cr.fill()

        # Menu right text 2
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 70, 16, 14, 2)
        cr.fill()

        # Menu right text 3
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 70, 24, 8, 2)
        cr.fill()

        # Menu right text 4
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 70, 32, 12, 2)
        cr.fill()

        # Menu right text 5
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 70, 40, 15, 2)
        cr.fill()

        # Menu right text 6
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 70, 48, 11, 2)
        cr.fill()

        # Menu right text 7
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 70, 56, 12, 2)
        cr.fill()

        # Menu right text 8
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 70, 64, 10, 2)
        cr.fill()

        # Whisker menu icon
        cr.set_line_width(2)
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 11, 3, 8, 8)
        cr.stroke()

        # Panel app text 1
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 11, 15, 8, 8)
        cr.fill()

        # Panel app text 2
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 11, 27, 8, 8)
        cr.fill()

        # Panel app text 3
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 11, 39, 8, 8)
        cr.fill()

        # Panel app text 4
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 11, 51, 8, 8)
        cr.fill()

        # Panel app text 5
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 11, 63, 8, 8)
        cr.fill()

        # System tray icons
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 11, LAYOUT_PREVIEW_HEIGHT - 27, 8, 18)
        cr.fill()

        # Clock
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 11, LAYOUT_PREVIEW_HEIGHT - 7, 8, 3)
        cr.fill()
