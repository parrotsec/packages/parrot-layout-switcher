"""
1 panel on top only
"""

import gi
from parrot_layout_switcher.platforms.xfce.layouts import *

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class XFCELayout(Layout, Gtk.Box):
    __metaclass__ = LayoutBox

    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        super().__init__()
        self.layout_settings = [
            {
                "settings": {"size": 32, "pos": "top_left"},
                "plugins": (
                    {"whisker_menu": {}},
                    {"places": {"show_button_type": 0}},
                    {"launcher": {"launcher_type": "terminal"}},
                    {"launcher": {"launcher_type": "web-browser"}},
                    {"workspace": {"rows": 2}},
                    {"task_list": {}},
                    {"separator": {"expand": True}},
                    {"pulseaudio": {}},
                    {"systray": {}},
                    {"clock": {}},
                )
            }
        ]
        self.settings = self.settings_to_xfconf()

        label = Gtk.Label()
        label.set_text("Top Panel")

        preview = Gtk.DrawingArea()
        preview.connect('draw', self.draw_layout_preview)
        preview.set_size_request(LAYOUT_PREVIEW_WIDTH, LAYOUT_PREVIEW_HEIGHT)

        self.pack_start(preview, True, False, 0)
        self.pack_start(label, True, False, 0)

    def draw_layout_preview(self, widget, cr):
        style_context = widget.get_style_context()
        color = style_context.get_color(Gtk.StateFlags.NORMAL)
        cr.set_source_rgba(*color)

        # Outline
        cr.set_line_width(2)
        cr.rectangle(1, 1, LAYOUT_PREVIEW_WIDTH - 2, LAYOUT_PREVIEW_HEIGHT - 2)
        cr.stroke()

        # Panel
        cr.set_line_width(2)
        cr.move_to(0, 9)
        cr.line_to(LAYOUT_PREVIEW_WIDTH, 9)
        cr.stroke()
        cr.move_to(0, 0)

        # Menu
        cr.set_line_width(2)
        cr.rectangle(5, LAYOUT_PREVIEW_HEIGHT - 167, 60, 70)
        cr.stroke()

        # Menu icon 1
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 164, 4, 4)
        cr.fill()

        # Menu text 1
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 163, 15, 2)
        cr.fill()

        # Menu icon 2
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 156, 4, 4)
        cr.fill()

        # Menu text 2
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 155, 10, 2)
        cr.fill()

        # Menu icon 3
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 147, 4, 4)
        cr.fill()

        # Menu text 3
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 146, 12, 2)
        cr.fill()

        # Menu icon 4
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 141, 4, 4)
        cr.fill()

        # Menu text 4
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 140, 11, 2)
        cr.fill()

        # Menu icon 5
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 133, 4, 4)
        cr.fill()

        # Menu text 5
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 132, 9, 2)
        cr.fill()

        # Menu icon 6
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 125, 4, 4)
        cr.fill()

        # Menu text 6
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 124, 14, 2)
        cr.fill()

        # Menu icon 7
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 117, 4, 4)
        cr.fill()

        # Menu text 7
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 116, 8, 2)
        cr.fill()

        # Menu icon 8
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 109, 4, 4)
        cr.fill()

        # Menu text 8
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 108, 10, 2)
        cr.fill()

        # Menu right text 1
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 163, 10, 2)
        cr.fill()

        # Menu right text 2
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 155, 14, 2)
        cr.fill()

        # Menu right text 3
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 147, 8, 2)
        cr.fill()

        # Menu right text 4
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 139, 12, 2)
        cr.fill()

        # Menu right text 5
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 131, 15, 2)
        cr.fill()

        # Menu right text 6
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 123, 11, 2)
        cr.fill()

        # Menu right text 7
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 115, 12, 2)
        cr.fill()

        # Menu right text 8
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 107, 10, 2)
        cr.fill()

        # Panel menu icon
        cr.set_line_width(2)
        cr.rectangle(3, LAYOUT_PREVIEW_HEIGHT - 178, 6, 6)
        cr.stroke()

        # Panel app text 1
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 176, 32, 2)
        cr.fill()

        # Panel app text 2
        cr.rectangle(50, LAYOUT_PREVIEW_HEIGHT - 176, 32, 2)
        cr.fill()

        # Panel app text 3
        cr.rectangle(86, LAYOUT_PREVIEW_HEIGHT - 176, 32, 2)
        cr.fill()

        # Indicators
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 34, LAYOUT_PREVIEW_HEIGHT - 176, 30, 2)
        cr.fill()
