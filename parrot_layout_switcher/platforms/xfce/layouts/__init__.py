# This file is part of the Zorin Appearance program.
#
# Copyright 2016-2019 Zorin OS Technologies Ltd.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
import gi
from os import listdir
from os.path import splitext
from abc import ABCMeta, abstractmethod
from parrot_layout_switcher.platforms.xfce import xfconf
from parrot_layout_switcher.platforms.xfce.handlers import themes, configs  # , backup

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, GLib
gi.require_version("Gdk", "3.0")
from gi.repository import Gdk
import importlib
from parrot_layout_switcher.platforms.xfce import items

LAYOUT_PREVIEW_WIDTH, LAYOUT_PREVIEW_HEIGHT = 240, 180
XFCONF = xfconf.XfconfSetting()
PATH = items.__path__[0]
# TODO load generated settings when program start, avoid multiple generation times

class Layout:
    __metaclass__ = ABCMeta

    def __init__(self):
        # Passing args from dict https://stackoverflow.com/a/43238973
        self.primary_monitor = ""
        self.monitors = []
        self.get_monitor_info()
        self.whisker_menu_id = -1
        self.windows_theme = False
        self.settings = []
        self.show_title = True
        # A dummy layout settings in dict format
        # We use this to generate xfconf settings later
        # The settings is a list of dict, each element in list is settings of panel and plugins in it
        # Structure should look like this
        # [
        #     {
        #         "settings": {"mode": 0, "size": 32, "pos": "bottom_left"},
        #         "plugins": (
        #             {"whisker_menu": {}},
        #             {"places": {"show_button_type": 0}},
        #             {"launcher": {"launcher_type": "terminal"}},
        #             {"launcher": {"launcher_type": "web-browser"}},
        #             {"system_load": {}},
        #             {"task_list": {}},
        #             {"separator": {"expand": True}},
        #             {"pulseaudio": {}},
        #             {"systray": {}},
        #             {"clock": {}},
        #         )
        #     }
        # ]
        # This is a tuple of panels, each panel has dictionary of
        # "settings": dictionary of settings to pass to argv
        # "plugins": tuple of plugins, dictionaries
        # Each plugin has structure
        # "plugin-name": dictionary of settings, which passes to function
        # The id will be handled automatically using for loop with enumeration
        # The panel settings and plugin settings are dictionaries
        # which passes to args (check https://stackoverflow.com/a/43238973)
        self.layout_settings = []

    def get_monitor_info(self):
        # https://askubuntu.com/a/639501
        # https://hicham.fedorapeople.org/gi/python/Gdk-3.0.html#Gdk.Display
        display = Gdk.Display.get_default()
        # Get primary monitor's port
        self.primary_monitor = display.get_primary_monitor().get_model()
        number_monitors = display.get_n_monitors()
        if number_monitors != 1:
            for i in range(display.get_n_monitors()):
                current_monitor_name = display.get_monitor(i).get_model()
                if current_monitor_name != self.primary_monitor:
                    self.monitors.append(current_monitor_name)

    def settings_to_xfconf(self):
        last_plugin_id = 0
        import_path = "parrot_layout_switcher." + PATH.split("parrot_layout_switcher/")[1].replace("/", ".")
        settings_handle = importlib.import_module(import_path)
        result = []  # return result

        for panel_id, panel in enumerate(self.layout_settings):
            conf_panel_settings = panel["settings"]

            for plugin_id, plugin in enumerate(panel["plugins"]):
                plugin_name, plugin_settings = list(plugin.items())[0]
                if plugin_name == "whisker_menu":
                    self.whisker_menu_id = last_plugin_id + plugin_id + 1
                # https://stackoverflow.com/a/8790232 Use importlib to execute function
                exec_function = getattr(settings_handle, plugin_name)
                # https://stackoverflow.com/a/43238973 dict to argv
                result += exec_function(last_plugin_id + plugin_id + 1, **plugin_settings)

            # Generate panel configurations
            result += items.panel_settings(panel_id, **conf_panel_settings, monitor=self.primary_monitor)
            # Generate plugins dbus values
            result += items.plugins_dbus_config(plugin_numbers=plugin_id + 1,
                                                plugin_start=last_plugin_id + 1, panel_id=panel_id)
            last_plugin_id += + plugin_id + 1
        panel_id += 1

        # Add panels to other monitors
        i = 0  # Fix LocalVariable i at line 127
        if self.monitors:
            for i, monitor in enumerate(self.monitors):
                result += items.panel_settings(panel_id + i, size=32, monitor=monitor)
                # Add task_list
                result += items.task_list(last_plugin_id + i + 1)
                # Add plugin_dbus
                result += items.plugins_dbus_config(plugin_numbers=1,
                                                    plugin_start=last_plugin_id + i + 1, panel_id=panel_id + i)
            i += 1

        # Generate decorator and button layout
        result.append(items.button_layout())
        result.append(items.decoration_layout())
        # Generate panel dbus config
        result += items.panel_dbus_config(panel_id + i)
        return result

    @abstractmethod
    def draw_layout_preview(self, widget, cr):
        pass

    @abstractmethod
    def on_clicked(self):
        # Create backup first
        # if backup.need_backup():
        #     backup.save_backup(XFCONF.xfconf.GetAllProperties("xfce4-panel", ""))

        # Remove all old properties. I don't know the way to call reset without errors so i do in a loop
        for xconf_property in XFCONF.xfconf.GetAllProperties("xfce4-panel", ""):
            XFCONF.xfconf.ResetProperty("xfce4-panel", xconf_property, "")

        if not self.windows_theme:
            themes.set_linux_theme()
        else:
            themes.set_windows_theme()

        # Set new settings for layout
        for setting in self.settings:
            if setting[2] == "systemload":
                configs.sysload_config()
            elif setting[2] == "whiskermenu" and self.whisker_menu_id != -1:
                configs.whisker_menu_config(self.whisker_menu_id, self.show_title, self.windows_theme)
            XFCONF.xfconf.SetProperty(setting[0], setting[1], setting[2])

        session_bus = Gio.BusType.SESSION
        conn = Gio.bus_get_sync(session_bus, None)

        destination = 'org.xfce.Panel'
        path = '/org/xfce/Panel'
        interface = destination

        dbus_proxy = Gio.DBusProxy.new_sync(
            conn,
            0,
            None,
            destination,
            path,
            interface,
            None,
        )
        #  Crash when new panel is added likely fixed
        #  gi.repository.GLib.Error: g-dbus-error-quark: GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown:
        #  The name org.xfce.Panel was not provided by any .service files (2)
        dbus_proxy.call_sync('Terminate', GLib.Variant('(b)', ('xfce4-panel',)), 0, -1, None)

    def is_not_custom_layout(self):
        for setting in self.settings:
            # Syntax:
            #   setting[0]: channel
            #   setting[1]: property
            #   setting[2]: value
            if XFCONF.xfconf.PropertyExists(setting[0], setting[1]):
                if XFCONF.xfconf.GetProperty(setting[0], setting[1]) != setting[2]:
                    # When we compare settings, the value of desktop files in launchers will be different
                    # Because the launchers are saved with different name in .config/xfce4/panel/launcher-*
                    # So we skip this by "items" of plugin "launcher"
                    if not setting[1].endswith("/items"):
                        return False
            else:
                return False

        return True

    @abstractmethod
    def is_current_layout(self):
        # query all settings https://stackoverflow.com/a/59352248
        # debug commands dbus-send --session --dest=org.xfce.Panel --type=method_call --print-reply
        #   /org/xfce/Panel org.freedesktop.DBus.Introspectable.Introspect
        # dbus-send --session --dest=org.xfce.Xfconf --type=method_call --print-reply
        #   /org/xfce/Xfconf org.freedesktop.DBus.Introspectable.Introspect
        # <method name="GetProperty">
        #       <arg type="s" name="channel" direction="in"/>
        #       <arg type="s" name="property" direction="in"/>
        #       <arg type="v" name="value" direction="out"/>
        #     </method>
        # Get all properties of xfce4-panel (chanel) from XfConf: XFCONF.xfconf.GetAllProperties("xfce4-panel", "")
        return self.is_not_custom_layout()


class LayoutBox(type(Gtk.Box), type(Layout)):
    pass


def get_layouts(dir_path):
    return [splitext(x)[0] for x in listdir(dir_path) if x.endswith(".py") and not x.startswith("__")]
