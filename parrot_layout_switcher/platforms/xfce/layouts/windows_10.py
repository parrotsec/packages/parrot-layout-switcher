"""
Windows 10 style with theme
"""

import gi
from parrot_layout_switcher.platforms.xfce.layouts import *

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class XFCELayout(Layout, Gtk.Box):
    __metaclass__ = LayoutBox

    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)

        super().__init__()
        self.windows_theme = True
        self.show_title = False
        self.layout_settings = [
            {
                "settings": {"size": 40, "pos": "bottom_left"},
                "plugins": (
                    {"whisker_menu": {}},
                    {"launcher": {"launcher_type": "terminal"}},
                    {"launcher": {"launcher_type": "web-browser"}},
                    {"launcher": {"launcher_type": "file-manager"}},
                    {"task_list": {"show_labels": False, "grouping": True}},
                    {"separator": {"expand": True}},
                    {"pulseaudio": {}},
                    {"systray": {}},
                    {"clock": {"style": 1}},
                )
            }
        ]
        self.settings = self.settings_to_xfconf()

        label = Gtk.Label()
        label.set_text("Windows 10")

        preview = Gtk.DrawingArea()
        preview.connect('draw', self.draw_layout_preview)
        preview.set_size_request(LAYOUT_PREVIEW_WIDTH, LAYOUT_PREVIEW_HEIGHT)

        self.pack_start(preview, True, False, 0)
        self.pack_start(label, False, False, 0)

    def draw_layout_preview(self, widget, cr):
        style_context = widget.get_style_context()
        color = style_context.get_color(Gtk.StateFlags.NORMAL)
        cr.set_source_rgba(*color)

        # Outline
        cr.set_line_width(2)
        cr.rectangle(1, 1, LAYOUT_PREVIEW_WIDTH - 2, LAYOUT_PREVIEW_HEIGHT - 2)
        cr.stroke()

        # Panel
        cr.set_line_width(2)
        cr.move_to(2, LAYOUT_PREVIEW_HEIGHT - 15)
        cr.line_to(LAYOUT_PREVIEW_WIDTH - 2, LAYOUT_PREVIEW_HEIGHT - 15)
        cr.stroke()
        cr.move_to(0, 0)

        # Menu area
        cr.set_line_width(2)
        cr.rectangle(5, LAYOUT_PREVIEW_HEIGHT - 89, 60, 70)
        cr.stroke()

        # Menu icon 1
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 86, 4, 4)
        cr.fill()

        # Menu text 1
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 85, 15, 2)
        cr.fill()

        # Menu icon 2
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 78, 4, 4)
        cr.fill()

        # Menu text 2
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 77, 10, 2)
        cr.fill()

        # Menu icon 3
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 70, 4, 4)
        cr.fill()

        # Menu text 3
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 69, 12, 2)
        cr.fill()

        # Menu icon 4
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 62, 4, 4)
        cr.fill()

        # Menu text 4
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 61, 11, 2)
        cr.fill()

        # Menu icon 5
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 54, 4, 4)
        cr.fill()

        # Menu text 5
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 53, 9, 2)
        cr.fill()

        # Menu icon 6
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 46, 4, 4)
        cr.fill()

        # Menu text 6
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 45, 14, 2)
        cr.fill()

        # Menu icon 7
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 38, 4, 4)
        cr.fill()

        # Menu text 7
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 37, 8, 2)
        cr.fill()

        # Menu icon 8
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 30, 4, 4)
        cr.fill()

        # Menu text 8
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 29, 10, 2)
        cr.fill()

        # Menu right text 1
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 85, 10, 2)
        cr.fill()

        # Menu right text 2
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 77, 14, 2)
        cr.fill()

        # Menu right text 3
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 69, 8, 2)
        cr.fill()

        # Menu right text 4
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 61, 12, 2)
        cr.fill()

        # Menu right text 5
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 53, 15, 2)
        cr.fill()

        # Menu right text 6
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 45, 11, 2)
        cr.fill()

        # Menu right text 7
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 37, 12, 2)
        cr.fill()

        # Menu right text 8
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 29, 10, 2)
        cr.fill()

        # Panel menu icon
        cr.set_line_width(2)
        cr.rectangle(3, LAYOUT_PREVIEW_HEIGHT - 12, 8, 8)
        cr.stroke()

        # Panel app icon 1
        cr.rectangle(15, LAYOUT_PREVIEW_HEIGHT - 12, 8, 8)
        cr.fill()

        # Panel app icon 2
        cr.rectangle(27, LAYOUT_PREVIEW_HEIGHT - 12, 8, 8)
        cr.fill()

        # Panel app icon 3
        cr.rectangle(39, LAYOUT_PREVIEW_HEIGHT - 12, 8, 8)
        cr.fill()

        # Panel app text 1
        cr.rectangle(74, LAYOUT_PREVIEW_HEIGHT - 12, 32, 8)
        cr.fill()

        # Panel app text 2
        cr.rectangle(110, LAYOUT_PREVIEW_HEIGHT - 12, 32, 8)
        cr.fill()

        # Panel app text 3
        cr.rectangle(146, LAYOUT_PREVIEW_HEIGHT - 12, 32, 8)
        cr.fill()

        # Systray
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 34, LAYOUT_PREVIEW_HEIGHT - 12, 30, 8)
        cr.fill()
