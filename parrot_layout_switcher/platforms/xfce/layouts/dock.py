"""
Only bottom dock panel
"""

import gi
from parrot_layout_switcher.platforms.xfce.layouts import *

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class XFCELayout(Layout, Gtk.Box):
    __metaclass__ = LayoutBox

    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        super().__init__()
        self.show_title = False
        self.layout_settings = [
            {
                "settings": {"size": 52, "pos": "bottom_center", "full_length": False, "autohide": 1},
                "plugins": (
                    {"whisker_menu": {}},
                    {"launcher": {"launcher_type": "terminal"}},
                    {"launcher": {"launcher_type": "web-browser"}},
                    {"launcher": {"launcher_type": "file-manager"}},
                    {"task_list": {"show_labels": False, "grouping": True}},
                    {"separator": {"expand": True}},
                    {"pulseaudio": {}},
                    {"systray": {}},
                    {"clock": {"style": 2}},
                )
            }
        ]
        self.settings = self.settings_to_xfconf()

        label = Gtk.Label()
        label.set_text("Dock")

        preview = Gtk.DrawingArea()
        preview.connect('draw', self.draw_layout_preview)
        preview.set_size_request(LAYOUT_PREVIEW_WIDTH, LAYOUT_PREVIEW_HEIGHT)

        self.pack_start(preview, True, False, 0)
        self.pack_start(label, True, False, 0)

    def draw_layout_preview(self, widget, cr):
        style_context = widget.get_style_context()
        color = style_context.get_color(Gtk.StateFlags.NORMAL)
        cr.set_source_rgba(*color)

        # Outline
        cr.set_line_width(2)
        cr.rectangle(1, 1, LAYOUT_PREVIEW_WIDTH - 2, LAYOUT_PREVIEW_HEIGHT - 2)
        cr.stroke()

        # Panel line
        cr.set_line_width(2)
        cr.move_to(63, LAYOUT_PREVIEW_HEIGHT - 15)
        cr.line_to(LAYOUT_PREVIEW_WIDTH - 63, LAYOUT_PREVIEW_HEIGHT - 15)
        cr.stroke()
        cr.move_to(0, 0)

        # Panel vertical line left
        cr.set_line_width(2)
        cr.move_to(LAYOUT_PREVIEW_WIDTH - 63, LAYOUT_PREVIEW_HEIGHT - 15)
        cr.line_to(LAYOUT_PREVIEW_WIDTH - 63, LAYOUT_PREVIEW_HEIGHT)
        cr.stroke()
        cr.move_to(0, 0)

        # Panel vertical line right
        cr.set_line_width(2)
        cr.move_to(63, LAYOUT_PREVIEW_HEIGHT - 15)
        cr.line_to(63, LAYOUT_PREVIEW_HEIGHT)
        cr.stroke()
        cr.move_to(0, 0)

        # Whisker menu icon
        cr.set_line_width(2)
        cr.rectangle(68, LAYOUT_PREVIEW_HEIGHT - 12, 8, 8)
        cr.stroke()

        # Whisker menu area
        cr.set_line_width(2)
        cr.rectangle(68, LAYOUT_PREVIEW_HEIGHT - 89, 60, 70)
        cr.stroke()

        # Menu icon 1
        cr.rectangle(75, LAYOUT_PREVIEW_HEIGHT - 85, 4, 4)
        cr.fill()

        # Menu text 1
        cr.rectangle(81, LAYOUT_PREVIEW_HEIGHT - 84, 15, 2)
        cr.fill()

        # Menu icon 2
        cr.rectangle(75, LAYOUT_PREVIEW_HEIGHT - 77, 4, 4)
        cr.fill()

        # Menu text 2
        cr.rectangle(81, LAYOUT_PREVIEW_HEIGHT - 76, 10, 2)
        cr.fill()

        # Menu icon 3
        cr.rectangle(75, LAYOUT_PREVIEW_HEIGHT - 69, 4, 4)
        cr.fill()

        # Menu text 3
        cr.rectangle(81, LAYOUT_PREVIEW_HEIGHT - 68, 12, 2)
        cr.fill()

        # Menu icon 4
        cr.rectangle(75, LAYOUT_PREVIEW_HEIGHT - 61, 4, 4)
        cr.fill()

        # Menu text 4
        cr.rectangle(81, LAYOUT_PREVIEW_HEIGHT - 60, 11, 2)
        cr.fill()

        # Menu icon 5
        cr.rectangle(75, LAYOUT_PREVIEW_HEIGHT - 53, 4, 4)
        cr.fill()

        # Menu text 5
        cr.rectangle(81, LAYOUT_PREVIEW_HEIGHT - 52, 9, 2)
        cr.fill()

        # Menu icon 6
        cr.rectangle(75, LAYOUT_PREVIEW_HEIGHT - 45, 4, 4)
        cr.fill()

        # Menu text 6
        cr.rectangle(81, LAYOUT_PREVIEW_HEIGHT - 44, 14, 2)
        cr.fill()

        # Menu icon 7
        cr.rectangle(75, LAYOUT_PREVIEW_HEIGHT - 37, 4, 4)
        cr.fill()

        # Menu text 7
        cr.rectangle(81, LAYOUT_PREVIEW_HEIGHT - 36, 8, 2)
        cr.fill()

        # Menu icon 8
        cr.rectangle(75, LAYOUT_PREVIEW_HEIGHT - 29, 4, 4)
        cr.fill()

        # Menu text 8
        cr.rectangle(81, LAYOUT_PREVIEW_HEIGHT - 28, 10, 2)
        cr.fill()

        # Menu right text 1
        cr.rectangle(105, LAYOUT_PREVIEW_HEIGHT - 84, 10, 2)
        cr.fill()

        # Menu right text 2
        cr.rectangle(105, LAYOUT_PREVIEW_HEIGHT - 76, 14, 2)
        cr.fill()

        # Menu right text 3
        cr.rectangle(105, LAYOUT_PREVIEW_HEIGHT - 68, 8, 2)
        cr.fill()

        # Menu right text 4
        cr.rectangle(105, LAYOUT_PREVIEW_HEIGHT - 60, 12, 2)
        cr.fill()

        # Menu right text 5
        cr.rectangle(105, LAYOUT_PREVIEW_HEIGHT - 52, 15, 2)
        cr.fill()

        # Menu right text 6
        cr.rectangle(105, LAYOUT_PREVIEW_HEIGHT - 44, 11, 2)
        cr.fill()

        # Menu right text 7
        cr.rectangle(105, LAYOUT_PREVIEW_HEIGHT - 36, 12, 2)
        cr.fill()

        # Menu right text 8
        cr.rectangle(105, LAYOUT_PREVIEW_HEIGHT - 28, 10, 2)
        cr.fill()

        # Panel app icon 1
        cr.rectangle(80, LAYOUT_PREVIEW_HEIGHT - 12, 8, 8)
        cr.fill()

        # Panel app icon 2
        cr.rectangle(92, LAYOUT_PREVIEW_HEIGHT - 12, 8, 8)
        cr.fill()

        # Panel app icon 3
        cr.rectangle(104, LAYOUT_PREVIEW_HEIGHT - 12, 8, 8)
        cr.fill()

        # Panel app icon 4
        cr.rectangle(116, LAYOUT_PREVIEW_HEIGHT - 12, 8, 8)
        cr.fill()

        # Panel app icon 5
        cr.rectangle(128, LAYOUT_PREVIEW_HEIGHT - 12, 8, 8)
        cr.fill()

        # Panel app icon 2
        cr.rectangle(140, LAYOUT_PREVIEW_HEIGHT - 12, 8, 8)
        cr.fill()

        # Sys tray
        cr.rectangle(152, LAYOUT_PREVIEW_HEIGHT - 12, 18, 8)
        cr.fill()
