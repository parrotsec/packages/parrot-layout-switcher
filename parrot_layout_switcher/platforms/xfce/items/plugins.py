"""
Create settings in dict object for all plugins that layout changer supports
Values will be changed to dbus again
xfconf-query commands to know values and entries https://forum.xfce.org/viewtopic.php?id=8619
Plugins:
1. separator
2. task_list (Windows button)
3. whisker_menu. Values of whisker menu are stored in config files and no dbus support. To change settings,
    the code must overwrite all config files and restart panels
4. clock
5. pulse_audio
6. app_menu: applications menu. Commonly known as top left menu.
7. system_load: system monitor plugin. To remove uptime value, it has to replace config files (as same as whisker menu)
8. places: Places menu. Which should show shortcut to /home and other directories
9. systray: system tray area
10. launcher: Add launcher plugin with all desktop files in group.
"""

import os
import dbus

XFCONFIG_PATH = os.path.expanduser("~/.config/xfce4/panel/")
LAUNCHER_PATH = "/usr/share/applications/"


def separator(expand=False, style=0):
    """
    :param expand: Do we want separator expanded or a simple column?
    :param style: style ID of separator, should be 0 <=  style <= 3
    Style:
        0. Transparent: don't show anything
        1. Separator |
        2. Handle 3 small - (or dot like)
        3. Dots (show 5 or 6 dots)
    :return:
    """
    if 0 <= style <= 3:
        pass
    else:
        raise (ValueError, f"Invalid separator style {style}")
    return {
        "name": "separator",
        "style": style,
        "expand": expand
    }


def task_list(grouping=False, show_labels=True, include_all_monitors=False):
    """
    Show list of running processes on current monitor only.
    Usually used for bottom panel
    :param grouping: Each process is 1 object or in group
    :param show_labels: Show name or icons only
    :param include_all_monitors: All processes on all monitors are at 1 task_list. Better to use
        True when the code is not supported for all monitors
    :return:
    """
    return {
        "name": "tasklist",
        "grouping": 0 if not grouping else 1,
        "include-all-monitors": include_all_monitors,
        "include-all-workspaces": False,
        "show-labels": show_labels,
        "show-only-minimized": False,
        "show-wireframes": False,
        "sort-order": 4,
        "switch-workspace-on-unminimize": True,
    }
    # ('xfce4-panel', '/plugins/plugin-3/sort-order', 1),
    # ('xfce4-panel', '/plugins/plugin-3/show-only-minimized', False),
    # ('xfce4-panel', '/plugins/plugin-3/switch-workspace-on-unminimize', True),


def whisker_menu():
    """
    Applications menu with search button :)
    Usually used for bottom panel
    Custom icons doesn't show in xfconf-query
    :return:
    """

    return {
        "name": "whiskermenu",
    }


def clock(mode=2, style=0):
    """
    :param mode: Layout of clock to show in panel
        0. Analog
        1. Binary
        2. Digital
        3. Fuzzy
        4. LCD
    :param style: Hardcoded clock display format
        0. %H:%M Display time only. For the bottom bar
        1. %H:%M %a%n%d/%b/%Y display 2 lines
        2. %H:%M%n%b %d display 2 lines but only time and date
        3. %a %H:%M display 1 line for center.
    :return:
    """
    if not 0 <= mode <= 4:
        raise(ValueError, f"Invalid clock mode {mode}. Mode must be 0 <= mode <= 4")
    if not 0 <= style <= 3:
        raise(ValueError, f"Invalid format style mode {style}. Value must be 0 <= style <= 2")

    clock_format = {
        0: "%H:%M",
        1: "%H:%M %a%n%d/%b/%Y",
        2: "%H:%M%n%b %d",
        3: "%a %H:%M"
    }
    return {
        "name": "clock",
        "mode": mode,
        "digital-format": clock_format[style],
    }


def pulseaudio():
    return {
        "name": "pulseaudio",
        "enable-keyboard-shortcuts": True
    }


def app_menu():
    return {
        "name": "applicationsmenu",
        "button-icon": "distributor-logo-parrot",
        "button-title": "Applications",
        "custom-menu": False,
        "show-generic-names": False,
        "show-tooltips": True
    }


def system_load():
    """
    System monitor of xfce
    :return:
    """
    return {
        "name": "systemload"
    }


def workspace(rows=2):
    """
    Workspace switcher
    :return:
    """
    return {
        "name": "pager",
        "rows": rows,
    }


def places(show_button_type=2):
    """
    Places plugin
    :param show_button_type: int -> (0, 1, 2)
        0. icon only
        1. label only
        2. icon and label
    :return:
    """
    if not 0 <= show_button_type <= 2:
        raise ValueError(f"Invalid option {show_button_type}. Value must be 0 <= show_button_type <= 2")
    return {
        "name": "places",
        "show-button-type": show_button_type
    }


def systray():
    """
    System tray
    :return:
    """
    return {
        "name": "systray",
        "show-frame": False,
    }


def __get_terminals():
    """
    Check if terminal launchers are available, then craft list of launchers
    :return: list of launchers of terminal
    """
    launchers = (
        "xfce4-terminal.desktop",
        "parrot-root-terminal.desktop",
        "terminator.desktop",
        "byobu.desktop",
    )
    result = [x for x in launchers if os.path.isfile(LAUNCHER_PATH + x)]
    return result


def __get_browsers():
    """
    Check if browser launchers are available, then craft list of launchers
    :return: list of launchers of browser
    """
    launchers = (
        "firefox.desktop",
        "firefox-esr.desktop",
        "chromium.desktop",
        "tor-browser.desktop"
    )
    result = [x for x in launchers if os.path.isfile(LAUNCHER_PATH + x)]
    return result


def launcher(launcher_type="terminal"):
    """
    Add the launchers as plugin in the system. All launchers must be a list because the value is dbus.Array
    We can have group of launchers with the list
    Maybe can support more desktop launchers and dynamic generate for groups
    For now we are having 3 different launchers in parrot-skel
        launcher-9 -> terminal
        launcher-10 -> file manager
        launcher-29 -> Firefox profile
    :return:
    """

    dict_launchers = {
        "terminal": __get_terminals(),
        "file-manager": ["xfce4-file-manager.desktop"],
        "web-browser": __get_browsers()
    }

    return {
        "name": "launcher",
        "items": dbus.Array(dict_launchers[launcher_type], signature=dbus.Signature('v'), variant_level=1),
    }

# Launcher: https://forum.xfce.org/viewtopic.php?id=11713
