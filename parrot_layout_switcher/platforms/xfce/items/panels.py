"""
Generate all settings for panels and return dict object
The value will be generated to dbus values again and send to dbus
"""


def panel_settings(full_length=True, size=30, mode=0, autohide=0, pos="bottom_left", rows=1, monitor="Primary"):
    """
    Generate settings for xfce4 panel
    The code doesn't have args for select monitors nor support mulitple monitors. Xfconf-query values example
        /panels/panel-0/output-name HDMI-0 -> Define the monitor of panel
        /panels/panel-0/output-name Primary -> auto select primary monitor
    :param full_length: bool
        Panel is full length or not (dock like)
    :param size: int -> Size of panel (pixel)
    :param mode: int 0, 1, 2
        0: Horizontal
        1: Vertical: vertical text / label
        2: DeskBar: horizontal text / label but vertical panel
    :param autohide: auto hide mode of xfce panel
        0: Never
        1: Intelligently
        2: Always
    :param pos, string, in list of
        # Horizontal panel
        "top_left": "p=0;x=0;y=0",
        "top_right": "p=1;x=0;y=0",
        "top_center": "p=9;x=0;y=0",
        "bottom_left": "p=8;x=0;y=0",
        "bottom_right": "p=4;x=0;y=0",
        "bottom_center": "p=10;x=0;y=0",
        # Vertical panel
        "right_center": "p=3;x=0;y=0",
        "left_center": "p=7;x=0;y=0"
    :param rows: int -> how many rows that panel has. Likely this is for systray. need to check again
    :param monitor: string -> Monitor's port. This argv allows the tool to support multiple monitors
    :return:
    """
    dict_pos = {
        # Horizontal panel
        "top_left": "p=0;x=0;y=0",  # p=5 or p=6 has same result
        "top_right": "p=1;x=0;y=0",  # p=2 has same result
        "top_center": "p=9;x=0;y=0",
        "bottom_left": "p=8;x=0;y=0",
        "bottom_right": "p=4;x=0;y=0",
        "bottom_center": "p=10;x=0;y=0",
        # Vertical panel
        "right_center": "p=3;x=0;y=0",
        "left_center": "p=7;x=0;y=0"
    }

    try:
        selected_position = dict_pos[pos]
    except NameError:
        raise(ValueError, f"Invalid position {pos}")

    if not 0 <= autohide <= 2:
        raise(ValueError, f"Invalid autohide mode {autohide}. Value must be 0 <= autohide <= 2")

    return {
        "autohide-behavior": autohide,
        "disable-struts": 0,
        "length": 100 if full_length else 1,
        "length-adjust": True,
        "mode": mode,
        "nrows": rows,
        "position-locked": True,
        "size": size,
        "position": selected_position,
        "output-name": monitor
    }
