"""
Generate plugin configs from dict to dbus
"""
import dbus
from parrot_layout_switcher.platforms.xfce.items import panels, plugins

DEFAULT_NAME = "xfce4-panel"


def __plugin_configs(plugin_id, plugin_settings):
    """
    Generate full plugin settings for XFCE from dict
    :param plugin_id: int -> ID number in panel
    :param plugin_settings: Dict of settings.
    :return:
    """
    result = []
    DEFAULT_XFCONF_PATH = "/plugins/plugin-"
    if plugin_id <= 0:
        raise(ValueError, f"Invalid plugin id {plugin_id}. ID of plugin must > 0")

    result.append((DEFAULT_NAME, f"{DEFAULT_XFCONF_PATH}{plugin_id}", f"{plugin_settings['name']}"))
    # Remove Name so we can add other settings in a simple loop
    plugin_settings.pop("name")
    for key, value in plugin_settings.items():
        result.append((DEFAULT_NAME, f"{DEFAULT_XFCONF_PATH}{plugin_id}/{key}", value))

    return result


def __panel_configs(panel_id, dict_panel_settings):
    """
    Generate config for panels to dbus from dict
    :param panel_id: int -> id number of panel
    :param dict_panel_settings: dict format for all configs
    :return:
    """
    result = [('xfce4-panel', '/configver', 2)]
    DEFAULT_XFCONF_PATH = "/panels/panel-"

    for key, value in dict_panel_settings.items():
        result.append((DEFAULT_NAME, f"{DEFAULT_XFCONF_PATH}{panel_id}/{key}", value))

    return result


def panel_dbus_config(panel_numbers=1):
    """
    Create dbus config of how many panels we are having
    :param panel_numbers:
    :return:
    """
    return [('xfce4-panel', '/panels', dbus.UInt32(panel_numbers, variant_level=1))]


def plugins_dbus_config(plugin_numbers, plugin_start=1, panel_id=0):
    """
    Create dbus config of how many plugins are we having for each panels
    :param plugin_numbers: How many plugins we have at 1 panel
    :param plugin_start: The first plugin id number of panel. Change it when we have >= 2 panels
    :param panel_id: The panel that all plugins are at
    :return:
    """
    result = [dbus.Int32(i, variant_level=1) for i in range(plugin_start, plugin_numbers + plugin_start)]
    return (
        (
            'xfce4-panel',
            f'/panels/panel-{panel_id}/plugin-ids',
            dbus.Array(result, signature=dbus.Signature('v'), variant_level=1)
        ),
    )


def panel_settings(panel_id, full_length=True, size=30, mode=0, autohide=0, pos="bottom_left", rows=1, monitor="Primary"):
    """
    Convert settings of panel to dbus
    :param panel_id: id number of panel
    For all other params, please check panels module for full descriptions
    :param full_length:
    :param size:
    :param mode:
    :param autohide:
    :param pos:
    :param rows:
    :param monitor
    :return:
    """
    return __panel_configs(panel_id, panels.panel_settings(full_length, size, mode, autohide, pos, rows, monitor))


def separator(plugin_id, expand=False, style=0):
    """
    Convert settings of plugin to dbus
    :param plugin_id: int -> unique id of plugin in dbus
    For all other params, please check plugins module for full descriptions
    :param expand:
    :param style:
    :return:
    """
    return __plugin_configs(plugin_id, plugins.separator(expand, style))


def task_list(plugin_id, grouping=False, show_labels=True, include_all_monitors=False):
    """
    Convert settings of plugin to dbus
    :param plugin_id: int -> unique id of plugin in dbus
    For all other params, please check plugins module for full descriptions
    :param grouping:
    :param show_labels:
    :param include_all_monitors:
    :return:
    """
    return __plugin_configs(plugin_id, plugins.task_list(grouping, show_labels, include_all_monitors))


def whisker_menu(plugin_id):
    """
    Convert settings of plugin to dbus
    :param plugin_id: int -> unique id of plugin in dbus
    For all other params, please check plugins module for full descriptions
    :return:
    """
    return __plugin_configs(plugin_id, plugins.whisker_menu())


def clock(plugin_id, mode=2, style=0):
    """
    Convert settings of plugin to dbus
    :param plugin_id: int -> unique id of plugin in dbus
    For all other params, please check plugins module for full descriptions
    :param plugin_id:
    :param mode:
    :param style:
    :return:
    """
    return __plugin_configs(plugin_id, plugins.clock(mode, style))


def pulseaudio(plugin_id):
    """
    Convert settings of plugin to dbus
    :param plugin_id: int -> unique id of plugin in dbus
    For all other params, please check plugins module for full descriptions
    :return:
    """
    return __plugin_configs(plugin_id, plugins.pulseaudio())


def app_menu(plugin_id):
    """
    Convert settings of plugin to dbus
    :param plugin_id: int -> unique id of plugin in dbus
    For all other params, please check plugins module for full descriptions
    :param plugin_id:
    :return:
    """
    return __plugin_configs(plugin_id, plugins.app_menu())


def system_load(plugin_id):
    """
    Convert settings of plugin to dbus
    :param plugin_id: int -> unique id of plugin in dbus
    For all other params, please check plugins module for full descriptions
    :return:
    """
    return __plugin_configs(plugin_id, plugins.system_load())


def workspace(plugin_id, rows=2):
    """
    Convert settings of plugin to dbus
    :param plugin_id: int -> unique id of plugin in dbus
    For all other params, please check plugins module for full descriptions
    :param rows:
    :return:
    """
    return __plugin_configs(plugin_id, plugins.workspace(rows))


def places(plugin_id, show_button_type=2):
    """
    Convert settings of plugin to dbus
    :param plugin_id: int -> unique id of plugin in dbus
    For all other params, please check plugins module for full descriptions
    :param show_button_type
    :return:
    """
    return __plugin_configs(plugin_id, plugins.places(show_button_type))


def systray(plugin_id):
    """
    Convert settings of plugin to dbus
    :param plugin_id: int -> unique id of plugin in dbus
    For all other params, please check plugins module for full descriptions
    :return:
    """
    return __plugin_configs(plugin_id, plugins.systray())


def launcher(plugin_id, launcher_type="terminal"):
    """
    Convert settings of plugin to dbus
    :param plugin_id: int -> unique id of plugin in dbus
    For all other params, please check plugins module for full descriptions
    :param launcher_type:
    :return:
    """
    return __plugin_configs(plugin_id, plugins.launcher(launcher_type))


def button_layout():
    return 'xfwm4', '/general/button_layout', 'O|HMC'


def decoration_layout():
    return 'xsettings', '/Gtk/DecorationLayout', 'menu:minimize,maximize,close'
