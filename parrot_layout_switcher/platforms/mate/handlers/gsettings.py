#!/usr/bin/env python3

# No longer uses reuse code of mate tweak
# api:https://lazka.github.io/pgi-docs/#Gio-2.0/classes/Settings.html

from gi.repository import Gio
from parrot_layout_switcher.platforms.mate.handlers.configs import *


def init_gsettings(schema, path=""):
    if path:
        return Gio.Settings.new_with_path(schema, path)
    else:
        return Gio.Settings.new(schema)


def reset_key_rec(schema, path):
    gs = init_gsettings(schema, path)
    for key in gs:
        gs.reset(key)
    gs.apply()


def reset_layouts():
    def __do_restart_layout(gs_obj, id_list, sub_schema, sub_path):
        for subdir in gs_obj[id_list]:
            reset_key_rec(sub_schema, sub_path + subdir + "/")

    gs = init_gsettings(GSETTING_PANEL_SCHEMA)
    try:
        __do_restart_layout(gs, GSETTINGS_OBJECT_ID_LIST, GSETTINGS_OBJECT_SCHEMA, GSETTINGS_OBJECT_PATH)
    except Exception as error:
        print(f"Warn: restart layout objects {Exception} {error}")
    # Likely we can't not create new path here
    try:
        __do_restart_layout(gs, GSETTINGS_TOPLEVEl_ID_LIST, GSETTINGS_TOPLEVEL_SCHEMA, GSETTINGS_TOPLEVEL_PATH)
    except Exception as error:
        print(f"Warn: restart layout toplevels {Exception} {error}")
    gs["toplevel-id-list"] = [""]
    gs["object-id-list"] = [""]
    gs.apply()
    gs.sync() # Temp remove the sync. We only sync after applying all changes
