"""
Generate configurations to gsettings format
"""

AID_BRISK_MENU = "BriskMenuFactory::BriskMenu"
AID_NOTI_AREA = "NotificationAreaAppletFactory::NotificationArea"
AID_CLOCK_APPLET = "ClockAppletFactory::ClockApplet"
AID_WINDOW_LIST = "WnckletFactory::WindowListApplet"  # Task list?
AID_WORKSPACE_SWITCHER = "WnckletFactory::WorkspaceSwitcherApplet"
AID_SYS_MON = "MultiLoadAppletFactory::MultiLoadApplet"
AID_SLEEP_APPLET = "InhibitAppletFactory::InhibitApplet"
AID_DOCK_APPLET = "DockAppletFactory::DockApplet"

GSETTING_PANEL_SCHEMA = "org.mate.panel"
GSETTINGS_OBJECT_SCHEMA = "org.mate.panel.object"
GSETTINGS_TOPLEVEL_SCHEMA = "org.mate.panel.toplevel"
GSETTINGS_OBJECT_PATH = "/org/mate/panel/objects/"
GSETTINGS_TOPLEVEL_PATH = "/org/mate/panel/toplevels/"
GSETTINGS_OBJECT_ID_LIST = "object-id-list"
GSETTINGS_TOPLEVEl_ID_LIST = "toplevel-id-list"
GSETTINGS_TOPLEVEL_BOTTOM_PATH = "/org/mate/panel/toplevels/bottom/"
GSETTINGS_TOPLEVEL_TOP_PATH = "/org/mate/panel/toplevels/top/"
GSETTINGS_TOPLEVEL_LEFT_PATH = "/org/mate/panel/toplevels/left/"
GSETTINGS_TOPLEVEL_RIGHT_PATH = "/org/mate/panel/toplevels/right/"


def __validate_settings(options, default_values, required_items):
    # Check if options in argv are valid
    result = required_items
    for key, value in options.items():
        try:
            if default_values[key] == type(value):
                result[key] = value
            else:
                print(f"Mate's object configuration: Invalid key type {key} {type(value)} ")
        except Exception as error:
            print(f"Value error {error}")
    return result


def gsettings_object_settings(applet_iid, toplevel_id, options=None):
    """
    Example of workspace switcher
    # required
      applet-iid: 'WnckletFactory::WorkspaceSwitcherApplet'
      locked: false
      object_type="applet" # applet / launcher
      toplevel_id = "bottom" # panel's location
    # Optional: dict, which should have
      attached-toplevel-id: ''
      launcher-location: ''
      position: 1
      has-arrow: true
      custom-icon: ''
      action-type: 'none'
      menu-path: 'applications:/'
      tooltip: ''
      panel-right-stick: true
      use-menu-path: false
      use-custom-icon: false
    :return:
    """
    result = {
        "toplevel-id": toplevel_id,
        "applet-iid": applet_iid
    }

    if options:
        accepted_argv = {
            "attached-toplevel-id": str,
            "launcher-location": str,
            "has-arrow": bool,
            "custom-icon": str,
            "action-type": str,
            "menu-path": str,
            "tooltip": str,
            "panel-right-stick": bool,
            "use-menu-path": bool,
            "use-custom-icon": bool,
            "object-type": str,
            "locked": bool,
            "position": int,
        }
        # Check if options in argv are valid
        result = __validate_settings(options, accepted_argv, result)

    return result


def gsettings_toplevel_settings(orientation, options=None):
    """
    Required argv:
      orientation ("top", "bottom", "left", "right")
      expand (=True) For dock
      size (=26) Panel size
    Optional
      y-bottom: -1
      enable-buttons: false
      unhide-delay: 100
      animation-speed: 'fast'
      screen: 0 Xserver to display
      monitor: 0 Monitor to display (support multiple monitors)
      x: 0
      name: ''
      enable-arrows: true
      auto-hide-size: 1
      auto-hide: false
      y-centered: false
      y: 0 # y = 947 for bottom
      enable-animations: true
      hide-delay: 300
      x-centered: false
      x-right: -1
    :return:
    """
    result = {
        "orientation": orientation,
    }

    if options:
        accepted_argv = {
            "y-bottom": int,
            "enable-buttons": bool,
            "unhide-delay": int,
            "animation-speed": str,
            "screen": int,
            "monitor": int,
            "x": int,
            "name": str,
            "enable-arrows": bool,
            "auto-hide-size": int,
            "auto-hide": bool,
            "y-centered": bool,
            "y": int,
            "expand": bool,
            "size": int,
            "enable-animations": bool,
            "hide-delay": int,
            "x-centered": bool,
            "x-right": int,
        }
        # Check if options in argv are valid
        result = __validate_settings(options, accepted_argv, result)

    return result
