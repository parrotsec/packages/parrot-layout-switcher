# In mate, only clock, notification-area, window-list, workspace-switcher have specific name
# Other plugins have name "object-<id>" in gsettings
# Sysmon has specific schema "pref" (likely linked to other place)
# Specific settings for brisk menu is at com.solus-project.brisk-menu
# Reset object-<id> will remove it after logout and login
# Schema toplevel requires path
# Schema object (org.mate.panel.object) is valid, requires path /org/mate/panel/objects/
# Mate schemas and paths is in https://github.com/mate-desktop/mate-panel/blob/master/mate-panel/panel-schemas.h
# settings for launchers https://ubuntu-mate.community/t/19-10-loading-saving-custom-panel-layouts-is-utterly-broken/20340/13
# seems like panel_profile_find_new_id creates new subdir
# To get all id in objects, do print(Gio.Settings.new("org.mate.panel").get_value("object-id-list")). same key for toplevel
# useful command: $ gsettings list-recursive
# for loop can show id-list (check mate-tweak-helper #L59). Equal to Gio.Settings.new("org.mate.panel")["object-id-list"]
# README: python supports keys in schema (w/ path) as list control syntax. MUST CALL SYNC instead of only apply (yeah right!!!)
# User can change objects on panels using set_strv (set string array) to object-id-list. work randomly. remember to reset and apply
# When do settings for objects, user can set x = Gio.Settings.new("org.mate.panel.object", "/org/mate/panel/objects/object_<id>/") and then
# x.set_<type>(<key>, <value>)
"""
# Applet ids
notification-area -> 'NotificationAreaAppletFactory::NotificationArea'
clock -> 'ClockAppletFactory::ClockApplet'
window-list -> 'WnckletFactory::WindowListApplet'
workspace-switcher -> 'WnckletFactory::WorkspaceSwitcherApplet'
brisk-menu (object_1) -> 'BriskMenuFactory::BriskMenu'
system monitor (?) (object_2) -> 'MultiLoadAppletFactory::MultiLoadApplet'
The "sleep applet in top panel" (object_10) -> 'InhibitAppletFactory::InhibitApplet'
"""