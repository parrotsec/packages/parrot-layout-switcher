"""
Generate applets and other objects on Mate's panel
Each configuration has:
1. Schema (org.mate.panel.object)
2. Path (/org/mate/panel/objects/<name>/)
3. Settings (from mate.handles.configs.*)
"""
from parrot_layout_switcher.platforms.mate.handlers.configs import *


def __plugin_settings(obj_name, app_id, toplevel_id, settings):
    return {
        "schema": GSETTINGS_OBJECT_SCHEMA,
        "path": GSETTINGS_OBJECT_PATH + obj_name + "/",
        "id-name": obj_name,  # Use this to register to object-id-list
        "settings": gsettings_object_settings(app_id, toplevel_id, options=settings)
    }


def make_plugin_config(plugin_name, toplevel_id, settings):
    # if "locked" not in settings:
    #     settings["locked"] = True

    plugin_id = ""
    if plugin_name == "brisk_menu":
        settings["object-type"] = "applet"
        plugin_id = AID_BRISK_MENU
    elif plugin_name == "noti_area":
        settings["object-type"] = "applet"
        plugin_id = AID_NOTI_AREA
    elif plugin_name == "workspace_switcher":
        settings["object-type"] = "applet"
        plugin_id = AID_WORKSPACE_SWITCHER
    elif plugin_name == "sys_mon":
        settings["object-type"] = "applet"
        plugin_id = AID_SYS_MON
        # TODO set sysmon's color /org/mate/system-monitor/
    elif plugin_name == "clock":
        settings["object-type"] = "applet"
        plugin_id = AID_CLOCK_APPLET
    elif plugin_name == "sleep_applet":
        settings["object-type"] = "applet"
        plugin_id = AID_SLEEP_APPLET
    elif plugin_name == "window_list":
        settings["object-type"] = "applet"
        plugin_id = AID_WINDOW_LIST
    elif plugin_name == "classic_menu":
        plugin_id = ""
        settings["object-type"] = "menu-bar"
    elif plugin_name == "compact_menu":
        plugin_id = ""
        settings["object-type"] = "menu"
    elif plugin_name == "dock":
        plugin_id = AID_DOCK_APPLET
        # TODO custom dock settings
        settings["object-type"] = "applet"
    elif plugin_name.startswith("launcher"):
        settings["object-type"] = "launcher"

    # TODO (maybe) window selector. Similar to window list but only 1 icon for all processes
    # TODO volume control. Notification area has it, but how about dock?
    # TODO brightness applet / battery charge. Same reason with volume control

    return __plugin_settings(plugin_name, plugin_id, toplevel_id, settings)
