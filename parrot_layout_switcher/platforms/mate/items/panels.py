"""
Generate settings of panels (tolevel)
Each configuration has:
1. Schema (org.mate.panel.toplevel)
2. Path (/org/mate/panel/toplevels/<name>/)
3. Settings (from mate.handles.configs.*)

Use kwargs to get arguments, replace orientation by hardcoded value https://stackoverflow.com/a/8954974
(More: https://docs.python.org/3/tutorial/controlflow.html#unpacking-argument-lists)

Orientation (Panel position):
    Mate handles panel's position automatically using orientation. Supported orientation: top, bottom, left, right
    Data type: enum
Dock-like: expand = False
"""

from parrot_layout_switcher.platforms.mate.handlers.configs import *


def make_panel_configs(position, settings):
    path = ""
    # Default panel size of Parrot is 26
    if "size" not in settings:
        settings["size"] = 26

    if position == "bottom":
        path = GSETTINGS_TOPLEVEL_BOTTOM_PATH
    elif position == "top":
        path = GSETTINGS_TOPLEVEL_TOP_PATH
    elif position == "left":
        path = GSETTINGS_TOPLEVEL_LEFT_PATH
    elif position == "right":
        path = GSETTINGS_TOPLEVEL_RIGHT_PATH

    return {
        "schema": GSETTINGS_TOPLEVEL_SCHEMA,
        "path": path,
        "id-name": position,  # Register to toplevel-id-list
        "settings": gsettings_toplevel_settings(position, settings)
    }
