# This file is part of the Zorin Appearance program
# Forked module and edited for Mate layouts
# Copyright 2016-2019 Zorin OS Technologies Ltd.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# TODO support multiple monitors
# TODO support decoration and themes# TODO support backup

import gi
from os import listdir
from os.path import splitext
from abc import ABCMeta, abstractmethod
# from parrot_layout_switcher.platforms.xfce import xfconf
# from parrot_layout_switcher.platforms.xfce.handlers import themes, configs  # , backup

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, GLib
gi.require_version("Gdk", "3.0")
from gi.repository import Gdk
from parrot_layout_switcher.platforms.mate import items
from parrot_layout_switcher.platforms.mate.handlers.gsettings import *
from parrot_layout_switcher.platforms.mate.items.panels import *
from parrot_layout_switcher.platforms.mate.items.plugins import make_plugin_config

LAYOUT_PREVIEW_WIDTH, LAYOUT_PREVIEW_HEIGHT = 240, 180
PATH = items.__path__[0]


class Layout:
    __metaclass__ = ABCMeta

    def __init__(self):
        # Passing args from dict https://stackoverflow.com/a/43238973
        self.primary_monitor = ""
        self.monitors = []
        self.get_monitor_info()
        self.whisker_menu_id = -1
        self.windows_theme = False
        self.settings = []
        self.show_title = True
        self.toplevel_id_list = []
        self.object_id_list = []
        self.layout_settings = []

    def get_monitor_info(self):
        # https://askubuntu.com/a/639501
        # https://hicham.fedorapeople.org/gi/python/Gdk-3.0.html#Gdk.Display
        display = Gdk.Display.get_default()
        # Get primary monitor's port
        self.primary_monitor = display.get_primary_monitor().get_model()
        number_monitors = display.get_n_monitors()
        if number_monitors != 1:
            for i in range(display.get_n_monitors()):
                current_monitor_name = display.get_monitor(i).get_model()
                if current_monitor_name != self.primary_monitor:
                    self.monitors.append(current_monitor_name)

    def apply_applets(self):
        """
        for applet in applets:
            self.gs.init(applet["schema"], applet["path"])
            # wrong
            for key, value in applet["settings"].items():
                x = self.gs.init(applet["schema"], applet["path"] "/")
                x.set(key, value)
        :return:
        """
        pass

    def settings_to_gsettings(self):
        result = []

        for toplevel_configs in self.layout_settings:
            toplevel_gsettings = make_panel_configs(toplevel_configs["pos"], toplevel_configs["settings"])
            result.append(toplevel_gsettings)
            self.toplevel_id_list.append(toplevel_configs["pos"])  # use same position and name for 1 panel
            for obj in toplevel_configs["objects"]:
                object_gsettings = make_plugin_config(obj["plugin"], toplevel_configs["pos"], obj["settings"])
                result.append(object_gsettings)
                self.object_id_list.append(obj["plugin"])
        return result

    @abstractmethod
    def draw_layout_preview(self, widget, cr):
        pass

    @abstractmethod
    def on_clicked(self):
        """
        Workflow
        1. Create backup (not imp)
        2. Reset layouts
        3. Apply layout from list of settings
        4. Add object-id-list and toplevel-id-list
        5. Check and change theme
        6. Check multiple monitors
        7. Sync all configs
        :return:
        """
        # 2. reset layouts
        reset_layouts()

        # 3. Apply layout settings
        for setting in self.settings:
            gs = init_gsettings(setting["schema"], setting["path"])
            print(f"Openning {setting['schema']} {setting['path']}")
            for key, value in setting["settings"].items():
                gs[key] = value
                print(f"  {key} -> {value}")
            gs.apply()

        # 4. Add object-id-list and toplevel-id-list
        gs = init_gsettings("org.mate.panel")
        gs["object-id-list"] = self.object_id_list
        gs["toplevel-id-list"] = self.toplevel_id_list
        gs.apply()
        # 7. Sync all configs
        # panel isn't written, or objects aren't written
        gs.sync()

    def is_not_custom_layout(self):
        for setting in self.settings:
            gs = init_gsettings(setting["schema"], setting["path"])
            for key, value in setting["settings"].items():
                if gs[key] != value:
                    # print(f"{setting['schema']} {setting['path']}: {key} -> {gs[key]} != {value}")
                    return False
        return True

    @abstractmethod
    def is_current_layout(self):
        # print(self.settings)
        return self.is_not_custom_layout()


class LayoutBox(type(Gtk.Box), type(Layout)):
    pass


def get_layouts(dir_path):
    return [splitext(x)[0] for x in listdir(dir_path) if x.endswith(".py") and not x.startswith("__")]
