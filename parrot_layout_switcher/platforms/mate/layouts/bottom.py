"""
Traditional style of Parrot OS. 2 panels, 1 on top and 1 bottom
"""

import gi
from parrot_layout_switcher.platforms.mate.layouts import *

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class MateLayout(Layout, Gtk.Box):
    __metaclass__ = LayoutBox

    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        super().__init__()
        self.layout_settings = [
            # Bottom
            {
                "pos": "bottom",
                "settings": {"size": 26},
                "objects": (
                    {
                        "plugin": "brisk_menu",
                        "settings": {"position": 0},
                    },
                    {
                        "plugin": "window_list",
                        "settings": {"position": 80},
                    },
                    {
                        "plugin": "noti_area",
                        "settings": {"panel-right-stick": True, "position": 205},
                    },
                    {
                        "plugin": "clock",
                        "settings": {"panel-right-stick": True, "position": 125},
                    }
                )
            }
        ]

        self.settings = self.settings_to_gsettings()

        label = Gtk.Label()
        label.set_text("Bottom")

        preview = Gtk.DrawingArea()
        preview.connect('draw', self.draw_layout_preview)
        preview.set_size_request(LAYOUT_PREVIEW_WIDTH, LAYOUT_PREVIEW_HEIGHT)

        self.pack_start(preview, True, False, 0)
        self.pack_start(label, True, False, 0)

    def draw_layout_preview(self, widget, cr):
        style_context = widget.get_style_context()
        color = style_context.get_color(Gtk.StateFlags.NORMAL)
        cr.set_source_rgba(*color)

        # Outline
        cr.set_line_width(2)
        cr.rectangle(1, 1, LAYOUT_PREVIEW_WIDTH - 2, LAYOUT_PREVIEW_HEIGHT - 2)
        cr.stroke()

        # Draw top panel

        # Panel
        cr.set_line_width(2)
        cr.move_to(0, 9)
        cr.line_to(LAYOUT_PREVIEW_WIDTH, 9)
        cr.stroke()
        cr.move_to(0, 0)

        # Draw application menu

        # Applications menu icon
        cr.set_line_width(2)
        cr.rectangle(3, LAYOUT_PREVIEW_HEIGHT - 178, 21, 6)
        cr.stroke()

        # Menu area
        cr.set_line_width(2)
        cr.rectangle(3, LAYOUT_PREVIEW_HEIGHT - 167, 21, 58)
        cr.stroke()

        # Menu right text 1
        cr.rectangle(6, LAYOUT_PREVIEW_HEIGHT - 163, 10, 2)
        cr.fill()

        # Menu right text 2
        cr.rectangle(6, LAYOUT_PREVIEW_HEIGHT - 155, 14, 2)
        cr.fill()

        # Menu right text 3
        cr.rectangle(6, LAYOUT_PREVIEW_HEIGHT - 147, 8, 2)
        cr.fill()

        # Menu right text 4
        cr.rectangle(6, LAYOUT_PREVIEW_HEIGHT - 139, 12, 2)
        cr.fill()

        # Menu right text 5
        cr.rectangle(6, LAYOUT_PREVIEW_HEIGHT - 131, 15, 2)
        cr.fill()

        # Menu right text 6
        cr.rectangle(6, LAYOUT_PREVIEW_HEIGHT - 123, 11, 2)
        cr.fill()

        # Menu right text 7
        cr.rectangle(6, LAYOUT_PREVIEW_HEIGHT - 115, 12, 2)
        cr.fill()

        # Draw place menu

        # Place menu icon
        cr.set_line_width(2)
        cr.rectangle(28, LAYOUT_PREVIEW_HEIGHT - 178, 21, 6)
        cr.stroke()

        # Menu area
        cr.set_line_width(2)
        cr.rectangle(28, LAYOUT_PREVIEW_HEIGHT - 167, 21, 30)
        cr.stroke()

        # Menu right text 1
        cr.rectangle(28, LAYOUT_PREVIEW_HEIGHT - 163, 10, 2)
        cr.fill()

        # Menu right text 2
        cr.rectangle(28, LAYOUT_PREVIEW_HEIGHT - 155, 14, 2)
        cr.fill()

        # Menu right text 3
        cr.rectangle(28, LAYOUT_PREVIEW_HEIGHT - 147, 8, 2)
        cr.fill()

        # Clock
        cr.rectangle((LAYOUT_PREVIEW_WIDTH - 20) / 2, LAYOUT_PREVIEW_HEIGHT - 176, 20, 2)
        cr.fill()

        # Sys mon and sys tray
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 38, LAYOUT_PREVIEW_HEIGHT - 176, 34, 2)
        cr.fill()

        # Draw bottom panel

        # Panel
        cr.set_line_width(2)
        cr.move_to(2, LAYOUT_PREVIEW_HEIGHT - 9)
        cr.line_to(LAYOUT_PREVIEW_WIDTH - 2, LAYOUT_PREVIEW_HEIGHT - 9)
        cr.stroke()
        cr.move_to(0, 0)

        # Menu
        cr.set_line_width(2)
        cr.rectangle(5, LAYOUT_PREVIEW_HEIGHT - 83, 60, 70)
        cr.stroke()

        # Menu icon 1
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 80, 4, 4)
        cr.fill()

        # Menu text 1
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 79, 15, 2)
        cr.fill()

        # Menu icon 2
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 72, 4, 4)
        cr.fill()

        # Menu text 2
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 71, 10, 2)
        cr.fill()

        # Menu icon 3
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 64, 4, 4)
        cr.fill()

        # Menu text 3
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 63, 12, 2)
        cr.fill()

        # Menu icon 4
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 56, 4, 4)
        cr.fill()

        # Menu text 4
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 55, 11, 2)
        cr.fill()

        # Menu icon 5
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 48, 4, 4)
        cr.fill()

        # Menu text 5
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 47, 9, 2)
        cr.fill()

        # Menu icon 6
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 40, 4, 4)
        cr.fill()

        # Menu text 6
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 39, 14, 2)
        cr.fill()

        # Menu icon 7
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 32, 4, 4)
        cr.fill()

        # Menu text 7
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 31, 8, 2)
        cr.fill()

        # Menu icon 8
        cr.rectangle(8, LAYOUT_PREVIEW_HEIGHT - 24, 4, 4)
        cr.fill()

        # Menu text 8
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 23, 10, 2)
        cr.fill()

        # Menu right text 1
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 79, 10, 2)
        cr.fill()

        # Menu right text 2
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 71, 14, 2)
        cr.fill()

        # Menu right text 3
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 63, 8, 2)
        cr.fill()

        # Menu right text 4
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 55, 12, 2)
        cr.fill()

        # Menu right text 5
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 47, 15, 2)
        cr.fill()

        # Menu right text 6
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 39, 11, 2)
        cr.fill()

        # Menu right text 7
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 31, 12, 2)
        cr.fill()

        # Menu right text 8
        cr.rectangle(44, LAYOUT_PREVIEW_HEIGHT - 23, 10, 2)
        cr.fill()

        # Panel menu icon
        cr.set_line_width(2)
        cr.rectangle(3, LAYOUT_PREVIEW_HEIGHT - 8, 6, 6)
        cr.stroke()

        # Panel app text 1
        cr.rectangle(14, LAYOUT_PREVIEW_HEIGHT - 6, 32, 2)
        cr.fill()

        # Panel app text 2
        cr.rectangle(50, LAYOUT_PREVIEW_HEIGHT - 6, 32, 2)
        cr.fill()

        # Panel app text 3
        cr.rectangle(86, LAYOUT_PREVIEW_HEIGHT - 6, 32, 2)
        cr.fill()

        # Indicators
        cr.rectangle(LAYOUT_PREVIEW_WIDTH - 34,
                     LAYOUT_PREVIEW_HEIGHT - 6, 30, 2)
        cr.fill()
