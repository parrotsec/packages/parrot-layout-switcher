#
# Copyright 2016-2019 Zorin OS Technologies Ltd.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from parrot_layout_switcher.gui import desktop
WINDOW_WIDTH = 735
WINDOW_HEIGHT = 660


class LayoutChanger(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_resizable(False)
        self.set_size_request(WINDOW_WIDTH, WINDOW_HEIGHT)
        self.set_position(Gtk.WindowPosition.CENTER)

        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        self.set_titlebar(hb)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(vbox)

        desktop_scrolled = Gtk.ScrolledWindow()
        desktop_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=18)
        desktop_box.set_border_width(18)
        desktop_box.add(desktop.Layouts())
        desktop_scrolled.add(desktop_box)

        vbox.pack_start(desktop_scrolled, True, True, 0)
        self.show()


def main_layout():
    window = LayoutChanger()
    window.set_title('Parrot Layout Changer')
    window.connect('delete-event', Gtk.main_quit)
    window.connect('destroy', Gtk.main_quit)
    window.show_all()
    Gtk.main()
