# This file is part of the Zorin Appearance program.
#
# Copyright 2016-2019 Zorin OS Technologies Ltd.
# Based on code from gnome-tweak-tool by John Stowers, Copyright 2011.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.

import gi
import importlib
import os


gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class Layouts(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=12)

        self.flowbox = Gtk.FlowBox()
        self.flowbox.set_valign(Gtk.Align.START)
        self.flowbox.set_row_spacing(28)
        self.flowbox.set_column_spacing(28)
        self.flowbox.set_margin_start(41)
        self.flowbox.set_margin_end(41)
        self.flowbox.set_selection_mode(Gtk.SelectionMode.SINGLE)
        self.flowbox.connect('child-activated', self.on_layout_changed)

        if os.environ["XDG_CURRENT_DESKTOP"] == "XFCE":
            list_layouts = [
                "default_4_11",
                "default_4_10",
                "bottom_normal",
                "bottom_modern",
                "top_left",
                "top_right",
                "windows_10",
                "windows_11",
                "dock_xfce",
                "dock_and_top",
                "dock",
                "top_panel",
            ]

            for layout_name in list_layouts:
                module = importlib.import_module(f"parrot_layout_switcher.platforms.xfce.layouts.{layout_name}")
                if hasattr(module, "XFCELayout"):
                    self.flowbox.add(getattr(module, "XFCELayout")())
        elif os.environ["XDG_CURRENT_DESKTOP"] == "MATE":
            list_layouts = [
                "bottom",
                "default",
                "dock",
                "top_left",
            ]
            for layout_name in list_layouts:
                module = importlib.import_module(f"parrot_layout_switcher.platforms.mate.layouts.{layout_name}")
                if hasattr(module, "MateLayout"):
                    self.flowbox.add(getattr(module, "MateLayout")())

        self.flowbox.connect('show', self.select_current_layout)
        self.pack_start(self.flowbox, True, True, 0)

    def create_layout_option(self, layout, image):
        selectable = Gtk.Box()
        selectable.pack_start(image, True, False, 0)
        return selectable

    def select_current_layout(self, flowbox):
        index = 0
        for interface_layout in self.flowbox.get_children():
            if interface_layout.get_child().is_current_layout():
                self.flowbox.select_child(interface_layout)
                return
            index += 1
        pass

    def on_layout_changed(self, box, child):
        child.get_child().on_clicked()
