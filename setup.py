from setuptools import setup

setup(
    name='parrot-layout-switcher',
    version='0.1.0',
    packages=['parrot_layout_switcher', 'parrot_layout_switcher.gui', 'parrot_layout_switcher.platforms',
              'parrot_layout_switcher.platforms.xfce', 'parrot_layout_switcher.platforms.xfce.items',
              'parrot_layout_switcher.platforms.xfce.layouts', 'parrot_layout_switcher.platforms.xfce.handlers',
              'parrot_layout_switcher.platforms.xfce.resources'],
    install_requires=[
        'dbus',
        'gi'
    ],
    entry_points={
        'console_scripts': [
            'parrot-layout-switcher = parrot_layout_switcher.gui.main_layout:main_layout',
        ],
    },
    data_files=[
        ('share/applications/', ['launchers/parrot-layout-switcher.desktop']),
        ('lib/python3/dist-packages/parrot_layout_switcher/platforms/xfce/resources/',
         ['parrot_layout_switcher/platforms/xfce/resources/systemload.rc',
          'parrot_layout_switcher/platforms/xfce/resources/whiskermenu.rc'])
    ],

    url='https://nest.parrotsec.org/packages/parrot/parrot-layout-switcher',
    license='GPL-3',
    author='Nong Hoang Tu',
    author_email='dmknght@parrotsec.org',
    description='A fork of ZorinOS layout switcher to Parrot OS'
)
